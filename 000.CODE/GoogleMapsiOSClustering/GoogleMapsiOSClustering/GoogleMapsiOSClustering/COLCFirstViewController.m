//
//  COLCFirstViewController.m
//  googlemapsios_cchmapclustercontroller_clustering
//
//  Created by Brian Clear (gbxc) on 12/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import "COLCFirstViewController.h"

#import "CCHMapClusterController.h"
#import "CCHMapClusterControllerDelegate.h"

#import "DataReader.h"
#import "DataReaderDelegate.h"

#import "CCHMapClusterAnnotation.h"
#import "ClusterAnnotationView.h"

#import "Settings.h"

#import "CCHCenterOfMassMapClusterer.h"
#import "CCHNearCenterMapClusterer.h"
#import "CCHFadeInOutMapAnimator.h"

@interface COLCFirstViewController ()<MKMapViewDelegate,CCHMapClusterControllerDelegate,DataReaderDelegate>{
    BOOL _debugOn;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) CCHMapClusterController *mapClusterController;
@property (strong, nonatomic) DataReader *dataReader;

@property (strong, nonatomic) Settings *settings;

@property (strong, nonatomic) id<CCHMapClusterer> mapClusterer;
@property (strong, nonatomic) id<CCHMapAnimator> mapAnimator;

@end

@implementation COLCFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _debugOn = TRUE;
    
    
	// Do any additional setup after loading the view, typically from a nib.
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    // Set up map clustering
    self.mapClusterController = [[CCHMapClusterController alloc] initWithMapView:self.mapView];
    self.mapClusterController.delegate = self;
    
    // Read annotations
    self.dataReader = [[DataReader alloc] init];
    self.dataReader.delegate = self;
    
    // Settings
    [self resetSettings];
}

- (IBAction)resetSettings
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    Settings *settings = [[Settings alloc] init];
    [self updateWithSettings:settings];
}

- (void)updateWithSettings:(Settings *)settings
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    self.settings = settings;
    
    // Reset
    [self.dataReader stopReadingData];
    [self.mapClusterController removeAnnotations:self.mapClusterController.annotations.allObjects withCompletionHandler:NULL];
    for (id<MKOverlay> overlay in self.mapView.overlays) {
        [self.mapView removeOverlay:overlay];
    }
    
    // Map cluster controller settings
    self.mapClusterController.debuggingEnabled = settings.isDebuggingEnabled;
    self.mapClusterController.cellSize = settings.cellSize;
    self.mapClusterController.marginFactor = settings.marginFactor;
    
    if (settings.clusterer == SettingsClustererCenterOfMass) {
        self.mapClusterer = [[CCHCenterOfMassMapClusterer alloc] init];
    } else if (settings.clusterer == SettingsClustererNearCenter) {
        self.mapClusterer = [[CCHNearCenterMapClusterer alloc] init];
    }
    self.mapClusterController.clusterer = self.mapClusterer;
    
    if (settings.animator == SettingsAnimatorFadeInOut) {
        self.mapAnimator = [[CCHFadeInOutMapAnimator alloc] init];
    }
    self.mapClusterController.animator = self.mapAnimator;
    
    // Region and data
    MKCoordinateRegion region;
    if (self.settings.dataSet == SettingsDataSetBerlin) {
        // 5000+ items near Berlin
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(52.516221, 13.377829);
        region = MKCoordinateRegionMakeWithDistance(location, 45000, 45000);
        [self.dataReader startReadingBerlinData];
    } else {
        // 80000+ items in the US
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(39.833333, -98.583333);
        region = MKCoordinateRegionMakeWithDistance(location, 7000000, 7000000);
        [self.dataReader startReadingUSData];
    }
    self.mapView.region = region;
}

#pragma mark -
#pragma mark <#METHODS#>
#pragma mark -

- (void)didReceiveMemoryWarning
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark DataReaderDelegate
#pragma mark -

- (void)dataReader:(DataReader *)dataReader addAnnotations:(NSArray *)annotations
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    [self.mapClusterController addAnnotations:annotations withCompletionHandler:NULL];
}

#pragma mark -
#pragma mark CCHMapClusterControllerDelegate
#pragma mark -
- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController
      titleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    NSUInteger numAnnotations = mapClusterAnnotation.annotations.count;
    NSString *unit = numAnnotations > 1 ? @"annotations" : @"annotation";
    return [NSString stringWithFormat:@"%tu %@", numAnnotations, unit];
}

- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController
   subtitleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    
    NSUInteger numAnnotations = MIN(mapClusterAnnotation.annotations.count, 5);
    
    NSArray *annotations = [mapClusterAnnotation.annotations.allObjects subarrayWithRange:NSMakeRange(0, numAnnotations)];
    NSArray *titles = [annotations valueForKey:@"title"];
    return [titles componentsJoinedByString:@", "];
}

- (void)mapClusterController:(CCHMapClusterController *)mapClusterController
willReuseMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    
    ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[self.mapClusterController.mapView viewForAnnotation:mapClusterAnnotation];
    //IF THIS CRASHES here make sure mapView.delegate set in storyboard
    clusterAnnotationView.count = mapClusterAnnotation.annotations.count;
}


#pragma mark -
#pragma mark MKMapViewDelegate
#pragma mark -

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    MKAnnotationView *annotationView;
    
    if ([annotation isKindOfClass:CCHMapClusterAnnotation.class]) {
        static NSString *identifier = @"clusterAnnotation";
        //IF THIS CRASHES SAYING MKPinAnnotation make sure mapView.delegate set in storyboard
        ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (clusterAnnotationView) {
            clusterAnnotationView.annotation = annotation;
        } else {
            clusterAnnotationView = [[ClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            clusterAnnotationView.canShowCallout = YES;
        }
        
        CCHMapClusterAnnotation *clusterAnnotation = (CCHMapClusterAnnotation *)annotation;
        clusterAnnotationView.count = clusterAnnotation.annotations.count;
        annotationView = clusterAnnotationView;
    }
    
    return annotationView;
}
@end
