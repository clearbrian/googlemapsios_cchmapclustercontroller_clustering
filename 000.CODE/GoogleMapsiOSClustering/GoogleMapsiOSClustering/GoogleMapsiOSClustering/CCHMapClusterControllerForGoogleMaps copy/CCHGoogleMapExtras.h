//
//  CCHGoogleMapExtras.h
//  googlemapsios_cchmapclustercontroller_clustering
//
//  Created by Brian Clear (gbxc) on 12/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
//typedef struct {
//    double x;
//    double y;
//} MKMapPoint;
//
//typedef struct {
//    double width;
//    double height;
//} MKMapSize;
//
//typedef struct {
//    MKMapPoint origin;
//    MKMapSize size;
//} MKMapRect;


//#ifdef __cplusplus
//#define GOOGLE_EXTERN       extern "C" __attribute__((visibility ("default")))
//#else
//#define GOOGLE_EXTERN       extern __attribute__((visibility ("default")))
//#endif
//
//GOOGLE_EXTERN const MKMapRect MKMapRectNull NS_AVAILABLE(10_9, 4_0);


#pragma mark -
#pragma mark FROM MKGemoetry.h
#pragma mark -

//typedef struct {
//    CLLocationDegrees latitudeDelta;
//    CLLocationDegrees longitudeDelta;
//} GoogleCoordinateSpan;
//
//typedef struct {
//	CLLocationCoordinate2D center;
//	GoogleCoordinateSpan span;
//} GoogleCoordinateRegion;

@interface CCHGoogleMapExtras : NSObject

@end
