//
//  GMSMapView+MKMapViewMethods.h
//  googlemapsios_cchmapclustercontroller_clustering
//
//  Created by Brian Clear (gbxc) on 12/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
//#import "MKAnnotationView.h"
#import <MapKit/MapKit.h>
@interface GMSMapView (MKMapViewMethods)
- (MKAnnotationView *)viewForAnnotation:(id <MKAnnotation>)annotation;
@end
