//
//  GoogleAnnotation.h
//  googlemapsios_cchmapclustercontroller_clustering
//
//  Created by Brian Clear (gbxc) on 12/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import <Foundation/Foundation.h>


//a duplicate of @protocol MKAnnotation <NSObject>
//Google SDK doesnt split a marker into Annotation for info and View for display
@protocol GoogleAnnotation <NSObject>

// Center latitude and longitude of the annotion view.
// The implementation of this property must be KVO compliant.
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@optional

// Title and subtitle for use by selection UI.
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;

// Called as a result of dragging an annotation view.
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate NS_AVAILABLE(10_9, 4_0);

@end