//
//  CCHMapViewDelegateProxyForGoogleMaps.m
//  CCHMapClusterController
//
//  Copyright (C) 2013 Claus Höfele
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "CCHMapViewDelegateProxyForGoogleMaps.h"
#import <GoogleMaps/GoogleMaps.h>
@interface CCHMapViewDelegateProxyForGoogleMaps()

@property (nonatomic, weak) NSObject<GMSMapViewDelegate> *delegate;
@property (nonatomic, weak) NSObject<GMSMapViewDelegate> *target;
@property (nonatomic, weak) GMSMapView *mapView;

@end

@implementation CCHMapViewDelegateProxyForGoogleMaps

- (id)initWithMapView:(GMSMapView *)mapView delegate:(NSObject<GMSMapViewDelegate> *)delegate
{
    self = [super init];
    if (self) {
        _delegate = delegate;   // must be set before swapDelegates
        _mapView = mapView;
        _target = mapView.delegate;
        [self swapDelegates];
    }
    return self;
}

- (void)dealloc
{
    [self.mapView removeObserver:self forKeyPath:@"delegate"];
    self.mapView.delegate = self.target;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self.mapView removeObserver:self forKeyPath:@"delegate"];
    [self swapDelegates];
}

- (void)swapDelegates
{
    self.target = self.mapView.delegate;
    self.mapView.delegate = self;
    [self.mapView addObserver:self forKeyPath:@"delegate" options:NSKeyValueObservingOptionNew context:NULL];
}

- (id)forwardingTargetForSelector:(SEL)selector
{
    id forwardingTarget;
    
    if ([self.delegate respondsToSelector:selector]) {
        forwardingTarget = self.delegate;
    } else if ([self.target respondsToSelector:selector]) {
        forwardingTarget = self.target;
    } else {
        forwardingTarget = [super forwardingTargetForSelector:selector];
    }
    
    return forwardingTarget;
}

- (BOOL)respondsToSelector:(SEL)selector
{
    BOOL respondsToSelector;
    
    if ([self.delegate respondsToSelector:selector]) {
        respondsToSelector = YES;
    } else if ([self.target respondsToSelector:selector]) {
        respondsToSelector = YES;
    } else {
        respondsToSelector = [super respondsToSelector:selector];
    }
    
    return respondsToSelector;
}

@end
