//
//  GoogleGMSMapView.h
//  googlemapsios_cchmapclustercontroller_clustering
//
//  Created by Brian Clear (gbxc) on 12/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

//MKMapRect
#import <MapKit/MapKit.h>


//#import "MKAnnotationView.h"
@interface GoogleGMSMapView : GMSMapView

//to handle MKMapView we copy its methods
@property (nonatomic, readonly) NSArray *annotations;
@property (nonatomic, readonly) NSArray *overlays;
@property (nonatomic) MKMapRect visibleMapRect;
@property (nonatomic, copy) NSArray *selectedAnnotations;

- (void)addAnnotation:(id < MKAnnotation >)annotation;
- (void)addAnnotations:(NSArray *)annotations;
- (void)removeAnnotations:(NSArray *)annotations;

- (MKAnnotationView *)viewForAnnotation:(id <MKAnnotation>)annotation;
- (void)deselectAnnotation:(id < MKAnnotation >)annotation animated:(BOOL)animated;
- (NSSet *)annotationsInMapRect:(MKMapRect)mapRect;
- (void)setRegion:(MKCoordinateRegion)region animated:(BOOL)animated;

@property (nonatomic) CLLocationCoordinate2D centerCoordinate;
- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated;



- (CLLocationCoordinate2D)convertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view;

@property(nonatomic) MKCoordinateRegion region;

- (void)selectAnnotation:(id < MKAnnotation >)annotation animated:(BOOL)animated;
@end
