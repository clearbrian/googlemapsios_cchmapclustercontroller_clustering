//
//  COLCFirstViewController.m
//  MyGoogleMapsClustering
//
//  Created by Brian Clear (gbxc) on 13/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import "COLCFirstViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "SNMapRectangle.h"
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>

@interface COLCFirstViewController ()<GMSMapViewDelegate>{
    BOOL _debugOn;
    
}
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic, retain) NSString *currentPointTitle;

@end

@implementation COLCFirstViewController

- (void)viewDidLoad
{
     _debugOn= TRUE;
    [super viewDidLoad];
    
    //[self runTests];
    
}


-(void)runTests{
    //[self testIsBetweenValueOne];
    [self testRectangleContains];
    
    self.mapView.delegate = self;
    self.mapView.settings.compassButton = NO;
    
    self.mapView.settings.rotateGestures = FALSE;
    
	// Do any additional setup after loading the view, typically from a nib.
    
    //-----------------------------------------------------------------------------------
    
    
    GMSMutablePath *path = [GMSMutablePath path];
    [path addCoordinate:CLLocationCoordinate2DMake(50.0,2.0)];//pC
    [path addCoordinate:CLLocationCoordinate2DMake(52.0,2.0)];//pB
    [path addCoordinate:CLLocationCoordinate2DMake(52.0,0.0)];//pA
    
    [path addCoordinate:CLLocationCoordinate2DMake(50.0,0.0)];//pD
    
    [path addCoordinate:CLLocationCoordinate2DMake(50.0,2.0)];//p5
    
    //OUTLINE OF RECTANGLE
    GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
    rectangle.map = self.mapView;
    
    //FILLED RECTANGLE - if you use this is whole grid whole screen will be blue/one color cos no gaps in between
    //GMSPolygon *rectangle = [GMSPolygon polygonWithPath:path];
    //rectangle.map = self.mapView;
    
    
    
    
    
    //-----------------------------------------------------------------------------------
    
    GMSMutablePath *pathRIGHTSIDE = [GMSMutablePath path];
    [pathRIGHTSIDE addCoordinate:CLLocationCoordinate2DMake(50.0,2.0)];//pC
    [pathRIGHTSIDE addCoordinate:CLLocationCoordinate2DMake(52.0,2.0)];//pB
    
    GMSPolyline *lineRIGHTSIDE = [GMSPolyline polylineWithPath:pathRIGHTSIDE];
    lineRIGHTSIDE.strokeColor = [UIColor redColor];
    
    //lineRIGHTSIDE.map = self.mapView;
    //-----------------------------------------------------------------------------------
    GMSMutablePath *pathTOP = [GMSMutablePath path];
    
    [pathTOP addCoordinate:CLLocationCoordinate2DMake(52.0,2.0)];//pB
    [pathTOP addCoordinate:CLLocationCoordinate2DMake(52.0,0.0)];//pA
    
    GMSPolyline *lineTOP = [GMSPolyline polylineWithPath:pathTOP];
    lineTOP.strokeColor = [UIColor greenColor];
    
    //lineTOP.map = self.mapView;
    //-----------------------------------------------------------------------------------
    //LHS
    GMSMutablePath *path1 = [GMSMutablePath path];
    
    [path1 addCoordinate:CLLocationCoordinate2DMake(52.0,0.0)];//pA
    [path1 addCoordinate:CLLocationCoordinate2DMake(50.0,0.0)];//pD
    
    
    GMSPolyline *line1 = [GMSPolyline polylineWithPath:path1];
    line1.strokeColor = [UIColor darkGrayColor];
    
    //line1.map = self.mapView;
    //-----------------------------------------------------------------------------------
    //BOTTOM
    GMSMutablePath *path2 = [GMSMutablePath path];
    
    
    [path2 addCoordinate:CLLocationCoordinate2DMake(50.0,0.0)];//pD
    
    [path2 addCoordinate:CLLocationCoordinate2DMake(50.0,2.0)];//pC
    
    GMSPolyline *line2 = [GMSPolyline polylineWithPath:path2];
    line2.strokeColor = [UIColor yellowColor];
    
    //line2.map = self.mapView;
    //-----------------------------------------------------------------------------------
    
    
    //2nd cluster
    
    GMSMutablePath *path5 = [GMSMutablePath path];
    [path5 addCoordinate:CLLocationCoordinate2DMake(50.0,4.0)];//pC
    [path5 addCoordinate:CLLocationCoordinate2DMake(52.0,4.0)];//pB
    [path5 addCoordinate:CLLocationCoordinate2DMake(52.0,2.0)];//pA
    
    [path5 addCoordinate:CLLocationCoordinate2DMake(50.0,2.0)];//pD
    
    [path5 addCoordinate:CLLocationCoordinate2DMake(50.0,4.0)];//p5
    
    GMSPolyline *rectangle5 = [GMSPolyline polylineWithPath:path5];
    //rectangle5.map = self.mapView;
    
    
    //-----------------------------------------------------------------------------------
    //a 16x6 cluster on the equator
    GMSMutablePath *path16x16 = [GMSMutablePath path];
    [path16x16 addCoordinate:CLLocationCoordinate2DMake(0.0,0.0)];
    [path16x16 addCoordinate:CLLocationCoordinate2DMake(16.0,0.0)];
    [path16x16 addCoordinate:CLLocationCoordinate2DMake(16.0,16.0)];
    [path16x16 addCoordinate:CLLocationCoordinate2DMake(0.0,16.0)];
    [path16x16 addCoordinate:CLLocationCoordinate2DMake(0.0,0.0)];
    
    GMSPolyline *rectangle16x16 = [GMSPolyline polylineWithPath:path16x16];
    //rectangle16x16.map = self.mapView;
    
    //moved down and into method
    //    //a 32x32 cluster on the equator
    //    GMSMutablePath *path32x32 = [GMSMutablePath path];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(0.0,0.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(32.0,0.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(32.0,32.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(0.0,32.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(0.0,0.0)];
    //
    //    GMSPolyline *rectangle32x32 = [GMSPolyline polylineWithPath:path32x32];
    //    rectangle32x32.map = self.mapView;
    //-----------------------------------------------------------------------------------
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:0
                                                            longitude:0.0
                                                                 zoom:3
                                                              bearing:0
                                                         viewingAngle:0];
    
    
    GMSCameraUpdate * gmsCameraUpdate = [GMSCameraUpdate  setCamera:camera];
    [self.mapView moveCamera:gmsCameraUpdate];
    
    //    /*
    //     typedef struct {
    //
    //     /** Bottom left corner of the camera. */
    //    CLLocationCoordinate2D nearLeft;
    //
    //    /** Bottom right corner of the camera. */
    //    CLLocationCoordinate2D nearRight;
    //
    //    /** Far left corner of the camera. */
    //    CLLocationCoordinate2D farLeft;
    //
    //    /** Far right corner of the camera. */
    //    CLLocationCoordinate2D farRight;
    //} GMSVisibleRegion;
    
    //GMSVisibleRegion
    //    self.mapView.projection.visibleRegion
    
    
    //when describing a cluster you can pick any point and go clock wise or anti clock wise
    //but for consistency I start at [Sx,Sy] bottom left and then go clock wise to topleft, top right, bottomleft, back to bottom sight
    //Start bottom left [Sx, Sy] go clock wise to draw rectangle
    //a..b...c..d
    
    //X and Y axis are swapped, normally X is across the bottom and Y goes from top to bottom
    //but Locations are described lat, lng and lat is distance from the equator
    CLLocationDegrees latitudeSx = 0.0;
    CLLocationDegrees latitudeEx = 32.0;
    CLLocationDegrees latitudeSy = 0.0;
    CLLocationDegrees latitudeEy = 32.0;
    
    //bottom left [Sx, Sy]
    CLLocationCoordinate2D pA_ = CLLocationCoordinate2DMake(latitudeSx,latitudeSy);
    //.. moving clockwise
    
    //top left [Ex, Sy]
    CLLocationCoordinate2D pB_ = CLLocationCoordinate2DMake(latitudeEx,latitudeSy);
    
    //top right [Ex, Ey]
    CLLocationCoordinate2D pC_ = CLLocationCoordinate2DMake(latitudeEx,latitudeEy);
    
    //bottom right [Sx, Ey]
    CLLocationCoordinate2D pD_ = CLLocationCoordinate2DMake(latitudeSx,latitudeEy);
    
    //-----------------------------------------------------------------------------------
    //FIND CENTER OF RECTANGLE
    //-----------------------------------------------------------------------------------
    //OK
    //CLLocationCoordinate2D centercoord = [self findCenterOfCoordRectangleForPointspA:pA_ pB:pB_ pC:pC_ pD:pD_];
    //if(_debugOn)NSLog(@"%s centercoord:%f,%f", __PRETTY_FUNCTION__, centercoord.latitude, centercoord.longitude);
    
    //    //-----------------------------------------------------------------------------------
    //    //a 32x6 cluster on the equator
    //    GMSMutablePath *path32x32 = [GMSMutablePath path];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(0.0,0.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(32.0,0.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(32.0,32.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(0.0,32.0)];
    //    [path32x32 addCoordinate:CLLocationCoordinate2DMake(0.0,0.0)];
    //
    //    GMSPolyline *rectangle32x32 = [GMSPolyline polylineWithPath:path32x32];
    //    rectangle32x32.map = self.mapView;
    /*
     Numbers that divide into 180 (num degrees east or west or n or s of 0.0)
     and return a whole number - one we can use as a side of a cluster
     180	1	180             - too big whole western hemisphere/north half - only 4 on whole map
     180	2	90
     180	3	60
     180	4	45
     180	5	36
     180	6	30
     180	7	25.7142857142857
     180	8	22.5
     180	9	20
     180	10	18
     180	11	16.3636363636364
     180	12	15
     180	13	13.8461538461538
     180	15	12
     180	16	11.25
     180	17	10.5882352941176
     180	18	10
     180	19	9.47368421052632
     180	20	9
     180	21	8.57142857142857
     180	23	7.82608695652174
     180	24	7.5
     180	25	7.2
     
     180	30	6
     
     180	36	5
     
     180	45	4
     
     180	60	3
     ...
     180	90	2
     */
    
    //-----------------------------------------------------------------------------------
    //DRAW A CLUSTER GRID
    //-----------------------------------------------------------------------------------
    
    //[self drawRectangleAtpA:pA_ pB:pB_ pC:pC_ pD:pD_];
    
    
    //[self drawGridWithSide:180.0];//ok but only 4 sectors - if you use polyline its ok, polygon doesnt draw bottom polygons
    //[self drawGridWithSide:90.0];//doesnt draw bottom for polyline or polygon
    //[self drawGridWithSide:45.0];//lines appear and disappear
    [self drawGridWithSide:30.0];//ok - and all markers appearing in center
    //[self drawGridWithSide:20.0];//ok
    //[self drawGridWithSide:15.0];//ok
    if(_debugOn)NSLog(@"");
}


-(void)drawGridWithSide:(double)sizeOfClusterRectInDegrees{
    int clusterCount = 0;
    int clusterCountX = 0;

    for(double Sx = -180.0; Sx < 180.0; Sx = Sx + sizeOfClusterRectInDegrees)
    /*for(double Sx = 0.0; Sx < 180.0; Sx = Sx + sizeOfClusterRectInDegrees)*/
    {
        int clusterCountY = 0;
        for(double Sy = -180.0; Sy < 180.0; Sy = Sy + sizeOfClusterRectInDegrees)
        /*for(double Sy = 0.0; Sy < 180.0; Sy = Sy + sizeOfClusterRectInDegrees)*/
        {

            NSLog(@"%s Sx,Sy:%.2f,%.2f", __PRETTY_FUNCTION__, Sx, Sy);
            NSLog(@"%s CLUSTER:%d [x:%d,y:%d]===========================",__PRETTY_FUNCTION__, clusterCount, clusterCountX, clusterCountY);
            
            self.currentPointTitle = [NSString stringWithFormat:@"CLUSTER:%d [x:%d,y:%d]", clusterCount, clusterCountX, clusterCountY];
            //comment in to draw on of the clusters for debugging
            //starts at bottom right fils bottom row to left then moves up to next row
            // top right cluster is last
            if(72 == clusterCount){
                if(_debugOn)NSLog(@"");
            }
            [self drawRectangleAtSx:(double)Sx
                                 Sy:(double)Sy
                           WithSide:(double)sizeOfClusterRectInDegrees];




            
            clusterCount++;

            clusterCountY++;
            
            //-----------------------------------------------------------------------------------
            //test max number of
            //if(clusterCountY == 5) break;
            //-----------------------------------------------------------------------------------
        }
        clusterCountX++;
        
        
        //-----------------------------------------------------------------------------------
        //test max number of
        //if(clusterCountX == 5) break;
        //-----------------------------------------------------------------------------------
        
        if(_debugOn)NSLog(@"%s END OF ROW ===============================",__PRETTY_FUNCTION__);
    }
}

-(void)drawRectangleAtSx:(double)Sx
                      Sy:(double)Sy
                WithSide:(double)sizeOfClusterRectInDegrees
{
    CLLocationDegrees latitudeSx = Sx;
    CLLocationDegrees latitudeEx = Sx + sizeOfClusterRectInDegrees;
    CLLocationDegrees latitudeSy = Sy;
    CLLocationDegrees latitudeEy = Sy + sizeOfClusterRectInDegrees;
    
    //--------------------------
    //had problems with 180/-180 drew diagonal lines think it wrapped around
    if(-180.0 == latitudeSx){
        latitudeSx = -179.99;
    }
    if(180.0 == latitudeSx){
        latitudeSx = 179.99;
    }
    //--------------------------
    if(-180.0 == latitudeEx){
        latitudeEx = -179.99;
    }
    if(180.0 == latitudeEx){
        latitudeEx = 179.99;
    }
    //--------------------------
    if(-180.0 == latitudeSy){
        latitudeSy = -179.99;
    }
    if(180.0 == latitudeSy){
        latitudeSy = 179.99;
    }
    //--------------------------
    if(-180.0 == latitudeEy){
        latitudeEy = -179.99;
    }
    if(180.0 == latitudeEy){
        latitudeEy = 179.99;
    }
    //--------------------------
    
    //bottom left [Sx, Sy]
    CLLocationCoordinate2D pA_ = CLLocationCoordinate2DMake(latitudeSx,latitudeSy);
    //.. moving clockwise
    
    //top left [Ex, Sy]
    CLLocationCoordinate2D pB_ = CLLocationCoordinate2DMake(latitudeEx,latitudeSy);
    
    //top right [Ex, Ey]
    CLLocationCoordinate2D pC_ = CLLocationCoordinate2DMake(latitudeEx,latitudeEy);
    
    //bottom right [Sx, Ey]
    CLLocationCoordinate2D pD_ = CLLocationCoordinate2DMake(latitudeSx,latitudeEy);
    
    
    [self drawRectangleAtpA:pA_ pB:pB_ pC:pC_ pD:pD_];
}


-(void)drawRectangleAtpA:(CLLocationCoordinate2D)pA
                      pB:(CLLocationCoordinate2D)pB
                      pC:(CLLocationCoordinate2D)pC
                      pD:(CLLocationCoordinate2D)pD
{
    
    

    if(_debugOn)NSLog(@"%s pA:%.2f,%.2f", __PRETTY_FUNCTION__, pA.latitude, pA.longitude);
    if(_debugOn)NSLog(@"%s pB:%.2f,%.2f", __PRETTY_FUNCTION__, pB.latitude, pB.longitude);
    if(_debugOn)NSLog(@"%s pC:%.2f,%.2f", __PRETTY_FUNCTION__, pC.latitude, pC.longitude);
    if(_debugOn)NSLog(@"%s pD:%.2f,%.2f", __PRETTY_FUNCTION__, pD.latitude, pD.longitude);
    
    //-----------------------------------------------------------------------------------
    //UNFILLED RECTANGLE - draw 4 lines in a path
    //-----------------------------------------------------------------------------------
//OK
//    GMSMutablePath *path = [GMSMutablePath path];
//    [path addCoordinate:pA];
//    [path addCoordinate:pB];
//    [path addCoordinate:pC];
//    [path addCoordinate:pD];
//    [path addCoordinate:pA];//close the retangle
//    GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
//    rectangle.map = self.mapView;
    
    //-----------------------------------------------------------------------------------

    //OK - same a s above but avoids drawing same line on top of each other
    //when drawing a grid you only need RHS and BOTTOM
    GMSMutablePath *path = [GMSMutablePath path];

    //RHS - pB - pA
    [path addCoordinate:pB];
    [path addCoordinate:pA];
    //BOT12*12 - pA - pD
    [path addCoordinate:pD];
    
    GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
    rectangle.map = self.mapView;
    //-----------------------------------------------------------------------------------
//    //FILLED RECTANGLE - if you draw the whole grid then whole screen will be blue as stroke/fil both blue and no gaps
//    GMSMutablePath *path = [GMSMutablePath path];
//    [path addCoordinate:pA];
//    [path addCoordinate:pB];
//    [path addCoordinate:pC];
//    [path addCoordinate:pD];
//    //[path addCoordinate:pA];//close the retangle
//    GMSPolygon *rectangle = [GMSPolygon polygonWithPath:path];
//    rectangle.strokeColor = [UIColor redColor];
//    rectangle.map = self.mapView;
    
    
    
    //-----------------------------------------------------------------------------------
    //DEBUG - draw each side with different color
//    GMSMutablePath *pathLHS = [GMSMutablePath path];
//
//    [pathLHS addCoordinate:pA];
//    [pathLHS addCoordinate:pB];
//    
//    GMSPolyline *lineLHS = [GMSPolyline polylineWithPath:pathLHS];
//    lineLHS.strokeColor = [UIColor redColor];
//    
//    lineLHS.map = self.mapView;
    //-----------------------------------------------------------------------------------


    CLLocationCoordinate2D centercoord = [self findCenterOfCoordRectangleForPointspA:pA pB:pB pC:pC pD:pD];
    if(_debugOn)NSLog(@"%s centercoord:%f,%f", __PRETTY_FUNCTION__, centercoord.latitude, centercoord.longitude);
    
    GMSMarker *centerMarker_ = [[GMSMarker alloc] init];
    centerMarker_.title = self.currentPointTitle;

    //-----------------------------------------------------------------------------------

//    CGSize imageSize = CGSizeMake(20.0f, 20.0f);
//    UIImage *blankImage_ = [self blankUIImageOfSize:imageSize];
//    UIImage *textImage_ = [self drawText:@"HI!"
//                                 inImage:blankImage_
//                                 atPoint:CGPointMake(5.0,5.0)];
//    
//    
//    centerMarker_.icon = textImage_;
    //-----------------------------------------------------------------------------------
    //OK
    //centerMarker_.icon = [self imageFromText:@"104546646"];
    //-----------------------------------------------------------------------------------
    centerMarker_.icon = [self gradientImageInCircleFromTextBetter:@"9999"];

    centerMarker_.position = centercoord;
    //icon is usually a pin so bottom of pin is over lat, lng were showing
    //be were no using a circle so center it
    //this is a percentage value
    centerMarker_.groundAnchor = CGPointMake(0.5, 0.5);
    centerMarker_.map = self.mapView;
}

-(UIImage *)imageFromText:(NSString *)text
{
    
    
    // set the font type and size - iOS7 uses attributes
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.lineBreakMode = NSLineBreakByWordWrapping;
    textStyle.alignment = NSTextAlignmentCenter;
    
    UIFont *font = [UIFont systemFontOfSize:20.0];
    
    //DEPRECATED CGSize size  = [text sizeWithFont:font];
    CGSize size  = [text sizeWithAttributes:@{NSFontAttributeName:font,
                                              NSParagraphStyleAttributeName:textStyle}];
    
    //-----------------------------------------------------------------------------------

    
    // check if UIGraphicsBeginImageContextWithOptions is available (iOS is 4.0+)
    if (UIGraphicsBeginImageContextWithOptions != NULL)
        UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    else
        // iOS is < 4.0
        UIGraphicsBeginImageContext(size);
    
    // optional: add a shadow, to avoid clipping the shadow you should make the context size bigger
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [[UIColor brownColor] CGColor]);
    //-----------------------------------------------------------------------------------

    // draw in context, you can use also drawInRect:withFont:
    //DEPRECATED[text drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
    

    //configure text
    //UIFont *font = [UIFont boldSystemFontOfSize:12];
    //UIFont *textFont = [UIFont systemFontOfSize:16];
    

    //-----------------------------------------------------------------------------------
    //ios6
    ///[text drawInRect:CGRectIntegral(rect) withFont:font];
    // iOS 7 way
    [text drawAtPoint:CGPointMake(0.0, 0.0) withAttributes:@{NSFontAttributeName:font,
                                                           NSParagraphStyleAttributeName:textStyle}];
    
    //-----------------------------------------------------------------------------------

    //CGImageRef cimg = UIGraphicsGetCurrentContext();
    
    // transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    [image drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
    
    //CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(ctx, 2.0, 3.5, 5.0, 1.0);
    CGContextStrokeRect(ctx, rect);
    
    //EXPORT as UIImage
    UIImage *testImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();    
    
    return testImg;
}


#pragma mark -
#pragma mark MAPTIMIZE
#pragma mark -

static const size_t num_locations = 2;
static const CGFloat locations[2] = { 0.0, 1.0 };

//static const size_t num_locations = 3;
//static const CGFloat locations[3] = { 0.0, 0.90, 1.0 };

static const CGFloat components[5][8] = {
    {
        118.0 / 255., 209.0 / 255., 0.0 / 255., 1.0, // Start color
        118.0 / 255., 209.0 / 255., 0.0 / 255., 0.0  // End color
    },
    {
        255.0 / 255., 159.0 / 255., 0.0 / 255., 1.0, // Start color
        255.0 / 255., 159.0 / 255., 0.0 / 255., 0.0  // End color
    },
    {
        255.0 / 255., 105.0 / 255., 0.0 / 255., 1.0, // Start color
        255.0 / 255., 105.0 / 255., 0.0 / 255., 0.0  // End color
    },
    {
        240.0 / 255., 60.0 / 255., 0.0 / 255., 1.0, // Start color
        240.0 / 255., 60.0 / 255., 0.0 / 255., 0.0  // End color
    },
    {
        181.0 / 255., 0.0 / 255., 21.0 / 255., 1.0, // Start color
        181.0 / 255., 0.0 / 255., 21.0 / 255., 0.0  // End color
    }};

//from Maptimize - circle with fading gradient and text inside
-(UIImage *)gradientImageInCircleFromText:(NSString *)text
{

    // set the font type and size - iOS7 uses attributes
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.lineBreakMode = NSLineBreakByWordWrapping;
    textStyle.alignment = NSTextAlignmentCenter;
    
    UIFont *font = [UIFont boldSystemFontOfSize:15.0];
    
    //DEPRECATED CGSize size  = [text sizeWithFont:font];
    
    NSDictionary * fontAttributesDictionary_ = @{NSFontAttributeName:font,
                              NSParagraphStyleAttributeName:textStyle,
                              NSForegroundColorAttributeName:   [UIColor whiteColor]};
    
    CGSize textSize_  = [text sizeWithAttributes:fontAttributesDictionary_];
    

    CGFloat r = 2 * MAX(textSize_.width, textSize_.height) + 15;
    CGRect frame = CGRectMake(0, 0, r, r);
    
    NSUInteger _colorIndex;
    //pick color from array
    _colorIndex = 0;// green
    _colorIndex = 1;//yellow
    _colorIndex = 2;//orange
    _colorIndex = 3;
    _colorIndex = 4;//red
    
    //this method changed the gradient color based on length of the text as it was a number the more chars then higher the number
    //_colorIndex = MIN(4, [text length] - 1);
    
    //-----------------------------------------------------------------------------------
    //BC added
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, 1);
    //-----------------------------------------------------------------------------------

	CGContextRef context = UIGraphicsGetCurrentContext();
    
	CGContextSaveGState(context);
    //-----------------------------------------------------------------------------------
    //CONFIGURE THE GRADIENT
    CGGradientRef myGradient;
    CGColorSpaceRef myColorspace;
    
    myColorspace = CGColorSpaceCreateDeviceRGB();
    myGradient = CGGradientCreateWithColorComponents (myColorspace,
                                                      components[_colorIndex], /*set gradient color*/
                                                      locations,
                                                      num_locations);
	
	//CGPoint center = CGPointMake(rect.size.width / 2, rect.size.height / 2);
	CGPoint center = CGPointMake(frame.size.width / 2,
                                 frame.size.height / 2);
    /*
    void CGContextDrawRadialGradient(CGContextRef context,
                                     CGGradientRef gradient,
                                     CGPoint startCenter,
                                     CGFloat startRadius,
                                     CGPoint endCenter,
                                     CGFloat endRadius,
                                     CGGradientDrawingOptions options)
    */
	CGContextDrawRadialGradient(context, myGradient, center, 1, center, frame.size.width / 2, kCGGradientDrawsBeforeStartLocation);
	
	CGGradientRelease(myGradient);
    CGColorSpaceRelease(myColorspace);
    //-----------------------------------------------------------------------------------

    CGContextRestoreGState(context);
	CGContextSaveGState(context);
	
//	CGContextSetFillColor(context, components[_colorIndex]);
//    CGContextFillEllipseInRect(context, CGRectMake(frame.size.width / 4,
//                                                   frame.size.height / 4,
//                                                   frame.size.width / 2,
//                                                   frame.size.height / 2));
//    
//    CGContextRestoreGState(context);
//	
//	CGContextSaveGState(context);
	
	//NSString *title = [self.annotation title];
	//UIFont *font = [UIFont boldSystemFontOfSize:12];
	//CGSize tSize = [title sizeWithFont:font];
	
	//CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
	
    //BC
//    CGFloat fontColor[4] = {255,255,255,1};
//    CGContextSetFillColor(UIGraphicsGetCurrentContext(), fontColor);
//    CGContextSetTextDrawingMode(UIGraphicsGetCurrentContext(), kCGTextFillStroke);
//    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 2);
//    CGContextSetStrokeColorWithColor(UIGraphicsGetCurrentContext(), [[UIColor whiteColor] CGColor]);
    
//	[text drawInRect:CGRectMake(0,
//                                frame.size.height / 2 - textSize_.height / 2,
//                                frame.size.width,
//                                textSize_.height)
//			 withFont:font
//		lineBreakMode:NSLineBreakByClipping
//			alignment:NSTextAlignmentCenter];
    
//    CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);

    
    [text drawInRect:CGRectMake(0,
                                frame.size.height / 2 - textSize_.height / 2,
                                frame.size.width,
                                textSize_.height)
      withAttributes:fontAttributesDictionary_];
    
    
	
    CGContextRestoreGState(context);

    //EXPORT as UIImage
    UIImage *testImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return testImg;
}

//gradient designed in PaintCode
-(UIImage *)gradientImageInCircleFromTextBetter:(NSString *)text
{
    
    // set the font type and size - iOS7 uses attributes
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.lineBreakMode = NSLineBreakByWordWrapping;
    textStyle.alignment = NSTextAlignmentCenter;
    
    UIFont *font = [UIFont boldSystemFontOfSize:15.0];
    
    //DEPRECATED CGSize size  = [text sizeWithFont:font];
    
    NSDictionary * fontAttributesDictionary_ = @{NSFontAttributeName:font,
                                                 NSParagraphStyleAttributeName:textStyle,
                                                 NSForegroundColorAttributeName:   [UIColor whiteColor]};
    
    CGSize textSize_  = [text sizeWithAttributes:fontAttributesDictionary_];
    
    
    CGFloat radius_ = 2 * MAX(textSize_.width, textSize_.height) + 15;
    CGRect frame = CGRectMake(0, 0, radius_, radius_);
    

    
    //-----------------------------------------------------------------------------------
    //BC added
    UIGraphicsBeginImageContextWithOptions(frame.size, NO, 1);
    //-----------------------------------------------------------------------------------
    
	CGContextRef context = UIGraphicsGetCurrentContext();
    
	CGContextSaveGState(context);
    //-----------------------------------------------------------------------------------
    //CONFIGURE THE GRADIENT

    //-----------------------------------------------------------------------------------
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

    //// Color Declarations
    UIColor* gradientColor2 = [UIColor colorWithRed: 0.127 green: 0.557 blue: 0.623 alpha: 1];
    UIColor* gradientColor3 = [UIColor colorWithRed: 0.62 green: 0.766 blue: 0.779 alpha: 0.1];
    
    //// Gradient Declarations
    NSArray* gradientColors = [NSArray arrayWithObjects:
                               (id)gradientColor2.CGColor,
                               (id)[UIColor colorWithRed: 0.373 green: 0.662 blue: 0.701 alpha: 0.557].CGColor,
                               (id)gradientColor3.CGColor, nil];
    //Move stop positions of the gradient
    //to keep center dark so you can read the text keep gradientColor2 dark
    //and first stop as close to 0.5
    //dont set first to 0 this will start the gradient fading at the center poinr
    CGFloat gradientLocations[] = {0.56, 0.72, 1};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
    
    //-----------------------------------------------------------------------------------

    CGPoint center = CGPointMake(frame.size.width / 2,
                                 frame.size.height / 2);
    CGContextDrawRadialGradient(context,
                                gradient,
                                center, 1, center, frame.size.width / 2, kCGGradientDrawsBeforeStartLocation);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    //-----------------------------------------------------------------------------------
    
    CGContextRestoreGState(context);
	CGContextSaveGState(context);

    
    //-----------------------------------------------------------------------------------
    //DRAW THE TEXT
    [text drawInRect:CGRectMake(0,
                                frame.size.height / 2 - textSize_.height / 2,
                                frame.size.width,
                                textSize_.height)
      withAttributes:fontAttributesDictionary_];
    
    //-----------------------------------------------------------------------------------

    // Rectangle - around frame - for debugging if edge of gradient is blurry
//    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: frame];
//    [[UIColor blackColor] setStroke];
//    rectanglePath.lineWidth = 0.5;
//    [rectanglePath stroke];
    
    
    CGContextRestoreGState(context);
    
    //EXPORT as UIImage
    UIImage *testImg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return testImg;
}



#pragma mark -
#pragma mark BLANK IMAGE
#pragma mark -


- (UIImage *)blankUIImageOfSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 1);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextClearRect(ctx, CGRectMake(0, 0, size.width, size.height));
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{

    
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    //-----------------------------------------------------------------------------------
    //configure text
    UIFont *font = [UIFont boldSystemFontOfSize:12];
    //UIFont *textFont = [UIFont systemFontOfSize:16];
    
    NSMutableParagraphStyle *textStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    textStyle.lineBreakMode = NSLineBreakByWordWrapping;
    textStyle.alignment = NSTextAlignmentCenter;
    //-----------------------------------------------------------------------------------
    //ios6
    ///[text drawInRect:CGRectIntegral(rect) withFont:font];
    // iOS 7 way
    [text drawInRect:CGRectIntegral(rect) withAttributes:@{NSFontAttributeName:font,
                                                           NSParagraphStyleAttributeName:textStyle}];

    //-----------------------------------------------------------------------------------


    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

-(CLLocationCoordinate2D)OLDfindCenterOfCoordRectangleForPointspA:(CLLocationCoordinate2D)pA
                                      pB:(CLLocationCoordinate2D)pB
                                      pC:(CLLocationCoordinate2D)pC
                                      pD:(CLLocationCoordinate2D)pD
{
    CLLocationDegrees minLat,minLng,maxLat,maxLng;
    //set it to first point - will be overwritten by next few tests
    minLat = pC.latitude;
    minLng = pC.longitude;
    maxLat = pC.latitude;
    maxLng = pC.longitude;
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, pC.latitude);
    minLng = MIN(minLng, pC.longitude);
    
    maxLat = MAX(maxLat, pC.latitude);
    maxLng = MAX(maxLng, pC.longitude);
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, pB.latitude);
    minLng = MIN(minLng, pB.longitude);
    
    maxLat = MAX(maxLat, pB.latitude);
    maxLng = MAX(maxLng, pB.longitude);
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, pA.latitude);
    minLng = MIN(minLng, pA.longitude);
    
    maxLat = MAX(maxLat, pD.latitude);
    maxLng = MAX(maxLng, pD.longitude);
    //-----------------------------------------------------------------------------------
    
    if(_debugOn)NSLog(@"%s minLat:%f", __PRETTY_FUNCTION__, minLat);
    if(_debugOn)NSLog(@"%s minLng:%f", __PRETTY_FUNCTION__, minLng);
    if(_debugOn)NSLog(@"%s maxLat:%f", __PRETTY_FUNCTION__, maxLat);
    if(_debugOn)NSLog(@"%s maxLng:%f", __PRETTY_FUNCTION__, maxLng);
    

    CLLocationCoordinate2D coordinateOrigin = CLLocationCoordinate2DMake(minLat, minLng);
    CLLocationCoordinate2D coordinateMax = CLLocationCoordinate2DMake(maxLat, maxLng);
    
    MKMapPoint upperLeft = MKMapPointForCoordinate(coordinateOrigin);
    MKMapPoint lowerRight = MKMapPointForCoordinate(coordinateMax);
    
    //Create the map rect
    MKMapRect mapRect = MKMapRectMake(upperLeft.x,
                                      upperLeft.y,
                                      lowerRight.x - upperLeft.x,
                                      lowerRight.y - upperLeft.y);
    
    //Create the region
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //THIS HAS THE CENTER, it should include spread
    CLLocationCoordinate2D centerCoordinate = region.center;
    
    if(_debugOn)NSLog(@"%s centerCoordinate:[%f,%f]", __PRETTY_FUNCTION__, centerCoordinate.latitude, centerCoordinate.longitude);
    return centerCoordinate;
}


-(CLLocationCoordinate2D)findCenterOfCoordRectangleForPointspA:(CLLocationCoordinate2D)pA
                                                            pB:(CLLocationCoordinate2D)pB
                                                            pC:(CLLocationCoordinate2D)pC
                                                            pD:(CLLocationCoordinate2D)pD
{
    CLLocationDegrees minLat,minLng,maxLat,maxLng;
    //set it to first point - will be overwritten by next few tests
    minLat = pC.latitude;
    minLng = pC.longitude;
    maxLat = pC.latitude;
    maxLng = pC.longitude;
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, pC.latitude);
    minLng = MIN(minLng, pC.longitude);
    
    maxLat = MAX(maxLat, pC.latitude);
    maxLng = MAX(maxLng, pC.longitude);
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, pB.latitude);
    minLng = MIN(minLng, pB.longitude);
    
    maxLat = MAX(maxLat, pB.latitude);
    maxLng = MAX(maxLng, pB.longitude);
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, pA.latitude);
    minLng = MIN(minLng, pA.longitude);
    
    maxLat = MAX(maxLat, pD.latitude);
    maxLng = MAX(maxLng, pD.longitude);
    //-----------------------------------------------------------------------------------
    
    if(_debugOn)NSLog(@"%s LATs:", __PRETTY_FUNCTION__);
    if(_debugOn)NSLog(@"%s minLat:%f", __PRETTY_FUNCTION__, minLat);
    if(_debugOn)NSLog(@"%s maxLat:%f", __PRETTY_FUNCTION__, maxLat);
    
    if(_debugOn)NSLog(@"%s LNGs:", __PRETTY_FUNCTION__);
    if(_debugOn)NSLog(@"%s minLng:%f", __PRETTY_FUNCTION__, minLng);
    if(_debugOn)NSLog(@"%s maxLng:%f", __PRETTY_FUNCTION__, maxLng);
    
    
    CLLocationDegrees midLat = 0.0,midLng = 0.0;
    if(minLat < 0.0){
        //minLat < 0.0
        //-----------------------------------------------------------------------------------
        if((maxLat < 0.0))
        {
            //minLat < 0.0
            //maxLat < 0.0
            
            //minLat:-100.000000
            //maxLat:-80.000000
            
            //minLat:-179.990000
            //maxLat:-160.000000
            
            //get the positive diffe between the two neg numbers
            float diff = ((minLat * -1) - ((maxLat * -1)));
            //get half the diff
            float halfdiff = (diff/2);
            //and either add it to the lowest(next line) or subtract from highest
            midLat = minLat + halfdiff;
            if(_debugOn)NSLog(@"%s midLat:%f", __PRETTY_FUNCTION__, midLat);
            if(_debugOn)NSLog(@"");
        }else{
            //minLat < 0.0 including 0.0
            //maxLat >= 0.0
            //minLat:-20.000000
            //maxLat:0.000000
            
            
            //-20 >> 20 + 10 = 30
            float diff = (minLat * -1) + maxLat;
            //30/2 = 15
            float halfdiff = diff/2;
            //-15 = -20 + 15 = -5
            midLat = minLat + halfdiff;
            if(_debugOn)NSLog(@"%s midLat:%f", __PRETTY_FUNCTION__, midLat);
            if(_debugOn)NSLog(@"");
        }
        //-----------------------------------------------------------------------------------
        
    }else{
        //minLat > 0.0
        //-----------------------------------------------------------------------------------
        if((maxLat < 0.0))
        {
            //minLat > 0.0
            //maxLat < 0.0
            if(_debugOn)NSLog(@"");
            //never called prob cos clusters all line up on axis
        }else{
            //minLat >= 0.0 including 0.0
            //maxLat > 0.0
            float diff = maxLat - minLat;
            float halfdiff = diff/2;
            midLat = minLat + halfdiff;
            if(_debugOn)NSLog(@"%s midLat:%f", __PRETTY_FUNCTION__, midLat);
            if(_debugOn)NSLog(@"");
            
        }
        //-----------------------------------------------------------------------------------
    }
    
    //-----------------------------------------------------------------------------------
    //LNG
    //-----------------------------------------------------------------------------------
    if((minLng < 0.0))
    {
        //minLng < 0.0
        //-----------------------------------------------------------------------------------
        if((maxLng < 0.0))
        {
            //minLng < 0.0
            //maxLng < 0.0
            
            //minLng:-100.000000
            //maxLng:-80.000000
            
            //minLng:-179.990000
            //maxLng:-160.000000
            
            //get the positive diffe between the two neg numbers
            float diff = ((minLng * -1) - ((maxLng * -1)));
            //get half the diff
            float halfdiff = (diff/2);
            //and either add it to the lowest(next line) or subtract from highest
            midLng = minLng + halfdiff;
            if(_debugOn)NSLog(@"%s midLng:%f", __PRETTY_FUNCTION__, midLng);
            if(_debugOn)NSLog(@"");
        }else{
            //minLng < 0.0 including 0.0
            //maxLng >= 0.0
            //minLng:-20.000000
            //maxLng:0.000000
            
            
            //-20 >> 20 + 10 = 30
            float diff = (minLng * -1) + maxLng;
            //30/2 = 15
            float halfdiff = diff/2;
            //-15 = -20 + 15 = -5
            midLng = minLng + halfdiff;
            if(_debugOn)NSLog(@"%s midLng:%f", __PRETTY_FUNCTION__, midLng);
            if(_debugOn)NSLog(@"");
        }
        //-----------------------------------------------------------------------------------
        
    }else{
        //minLng > 0.0
        //-----------------------------------------------------------------------------------
        if((maxLng < 0.0))
        {
            //minLng > 0.0
            //maxLng < 0.0
            if(_debugOn)NSLog(@"");
            //never called prob cos clusters all line up on axis
        }else{
            //minLng >= 0.0 including 0.0
            //maxLng > 0.0
            float diff = maxLng - minLng;
            float halfdiff = diff/2;
            midLng = minLng + halfdiff;
            if(_debugOn)NSLog(@"%s midLng:%f", __PRETTY_FUNCTION__, midLng);
            if(_debugOn)NSLog(@"");
            
        }
        //-----------------------------------------------------------------------------------
    }
    //-----------------------------------------------------------------------------------
    if(_debugOn)NSLog(@"%s CENTER:", __PRETTY_FUNCTION__);
    if(_debugOn)NSLog(@"%s midLat, midLng:%f,%f", __PRETTY_FUNCTION__, midLat, midLng);

    if(_debugOn)NSLog(@"");
    //-----------------------------------------------------------------------------------


    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(midLat, midLng);
    
    if(_debugOn)NSLog(@"%s centerCoordinate:[%f,%f]", __PRETTY_FUNCTION__, centerCoordinate.latitude, centerCoordinate.longitude);
    return centerCoordinate;
}


- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    if(_debugOn)NSLog(@"%s position.zoom:%f", __PRETTY_FUNCTION__, position.zoom);
    
    //Rectangle starts at bottom left/nearleft/[Sx,Sy]

    //axis are swapped as compare to a maths graph
    //LAT = X axis - distance from eq - vertical/top to bottom axis
    //LNG = Y axis - distance from greenwich east/west - horzontal axis/left to right axis
    
//    //BOTTOMLEFT/nearLeft/pA/[Sx,Sy]
//    if(_debugOn)NSLog(@"%s MAP:  nearLeft:%f,%f", __PRETTY_FUNCTION__,
//          self.mapView.projection.visibleRegion.nearLeft.latitude,
//          self.mapView.projection.visibleRegion.nearLeft.longitude);
//    //TOP LEFT / farLeft / pB / [Ex, Sy]
//    if(_debugOn)NSLog(@"%s MAP:  farLeft:%f,%f", __PRETTY_FUNCTION__,
//                      self.mapView.projection.visibleRegion.farLeft.latitude,
//                      self.mapView.projection.visibleRegion.farLeft.longitude);
//    
//    //pC
//    if(_debugOn)NSLog(@"%s MAP: farRight:%f,%f", __PRETTY_FUNCTION__,
//                      self.mapView.projection.visibleRegion.farRight.latitude,
//                      self.mapView.projection.visibleRegion.farRight.longitude);
//
//    //pD
//    if(_debugOn)NSLog(@"%s MAP: nearRight:%f,%f", __PRETTY_FUNCTION__,
//                      self.mapView.projection.visibleRegion.nearRight.latitude,
//                      self.mapView.projection.visibleRegion.nearRight.longitude);
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)testRectangleContains{
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //CASE 1 - lng start - end -
    SNMapRectangle *snMapRectangleCol1a = [[SNMapRectangle alloc]init];
    snMapRectangleCol1a.pA = CLLocationCoordinate2DMake(100.0, -200.0);
    snMapRectangleCol1a.pB = CLLocationCoordinate2DMake(200.0, -200.0);
    snMapRectangleCol1a.pC = CLLocationCoordinate2DMake(200.0, -100.0);
    snMapRectangleCol1a.pD = CLLocationCoordinate2DMake(100.0, -100.0);
    
    //-----------------------------------------------------------------------------------

    SNMapRectangle *snMapRectangleINSIDE = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE.pA = CLLocationCoordinate2DMake( 160.0, -180.0);
    snMapRectangleINSIDE.pB = CLLocationCoordinate2DMake( 180.0, -180.0);
    snMapRectangleINSIDE.pC = CLLocationCoordinate2DMake( 180.0, -160.0);
    snMapRectangleINSIDE.pD = CLLocationCoordinate2DMake( 160.0, -160.0);
    
    if([snMapRectangleCol1a containsSNMapRectangle:snMapRectangleINSIDE]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    SNMapRectangle *snMapRectangleOUTSIDE = [[SNMapRectangle alloc]init];
    snMapRectangleOUTSIDE.pA = CLLocationCoordinate2DMake(-50.0, -200.0);
    snMapRectangleOUTSIDE.pB = CLLocationCoordinate2DMake( 50.0, -200.0);
    snMapRectangleOUTSIDE.pC = CLLocationCoordinate2DMake( 50.0, -100.0);
    snMapRectangleOUTSIDE.pD = CLLocationCoordinate2DMake(-50.0, -100.0);
    

    if([snMapRectangleCol1a containsSNMapRectangle:snMapRectangleOUTSIDE]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    
    
    //-----------------------------------------------------------------------------------
    //SPECIAL CASE - cluster outside but touching so two points(one side of the smaller cluster) classed as inside
    
    //Rectangle right beside snMapRectangleCol1a
    //outside but sides touch - so new rule is ALL 4 points must be inside to be considered inside an out cluster
    
    SNMapRectangle *snMapRectangleAGAINST = [[SNMapRectangle alloc]init];
    snMapRectangleAGAINST.pA = CLLocationCoordinate2DMake(100.0, -100.0);
    snMapRectangleAGAINST.pB = CLLocationCoordinate2DMake(200.0, -100.0);
    snMapRectangleAGAINST.pC = CLLocationCoordinate2DMake(200.0,    0.0);
    snMapRectangleAGAINST.pD = CLLocationCoordinate2DMake(100.0,    0.0);
    
    
    if([snMapRectangleCol1a containsSNMapRectangle:snMapRectangleAGAINST]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //CASE 2
    
    SNMapRectangle *snMapRectangleCol1b = [[SNMapRectangle alloc]init];
    snMapRectangleCol1b.pA = CLLocationCoordinate2DMake(-50.0, -200.0);
    snMapRectangleCol1b.pB = CLLocationCoordinate2DMake( 50.0, -200.0);
    snMapRectangleCol1b.pC = CLLocationCoordinate2DMake( 50.0, -100.0);
    snMapRectangleCol1b.pD = CLLocationCoordinate2DMake(-50.0, -100.0);
    
    //-----------------------------------------------------------------------------------
    //INSIDE
    SNMapRectangle *snMapRectangleINSIDE1 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE1.pA = CLLocationCoordinate2DMake( -10.0, -180.0);
    snMapRectangleINSIDE1.pB = CLLocationCoordinate2DMake(  10.0, -180.0);
    snMapRectangleINSIDE1.pC = CLLocationCoordinate2DMake(  10.0, -160.0);
    snMapRectangleINSIDE1.pD = CLLocationCoordinate2DMake( -10.0, -160.0);
    
    if([snMapRectangleCol1b containsSNMapRectangle:snMapRectangleINSIDE1]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    //-----------------------------------------------------------------------------------
    //OUTSIDE - just use 1st one - snMapRectangleCol1a
    
    if([snMapRectangleCol1b containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //CASE 3 - col 1 bottom
    SNMapRectangle *snMapRectangleCol1c = [[SNMapRectangle alloc]init];
    snMapRectangleCol1c.pA = CLLocationCoordinate2DMake(-200.0, -200.0);
    snMapRectangleCol1c.pB = CLLocationCoordinate2DMake(-100.0, -200.0);
    snMapRectangleCol1c.pC = CLLocationCoordinate2DMake(-100.0, -100.0);
    snMapRectangleCol1c.pD = CLLocationCoordinate2DMake(-200.0, -100.0);
    
    //-----------------------------------------------------------------------------------
    //INSIDE
    SNMapRectangle *snMapRectangleINSIDE2 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE2.pA = CLLocationCoordinate2DMake( -180.0, -180.0);
    snMapRectangleINSIDE2.pB = CLLocationCoordinate2DMake( -160.0, -180.0);
    snMapRectangleINSIDE2.pC = CLLocationCoordinate2DMake( -160.0, -160.0);
    snMapRectangleINSIDE2.pD = CLLocationCoordinate2DMake( -180.0, -160.0);
    
    if([snMapRectangleCol1c containsSNMapRectangle:snMapRectangleINSIDE2]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    //-----------------------------------------------------------------------------------
    //OUTSIDE
    if([snMapRectangleCol1c containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    
    }
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    //COL 2 - lng start - end +
    SNMapRectangle *snMapRectangleCol2a = [[SNMapRectangle alloc]init];
    snMapRectangleCol2a.pA = CLLocationCoordinate2DMake(100.0, -50.0);
    snMapRectangleCol2a.pB = CLLocationCoordinate2DMake(200.0, -50.0);
    snMapRectangleCol2a.pC = CLLocationCoordinate2DMake(200.0,  50.0);
    snMapRectangleCol2a.pD = CLLocationCoordinate2DMake(100.0,  50.0);
    
    
    SNMapRectangle *snMapRectangleINSIDE3 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE3.pA = CLLocationCoordinate2DMake( 160.0, -25.0);
    snMapRectangleINSIDE3.pB = CLLocationCoordinate2DMake( 180.0, -25.0);
    snMapRectangleINSIDE3.pC = CLLocationCoordinate2DMake( 180.0,  25.0);
    snMapRectangleINSIDE3.pD = CLLocationCoordinate2DMake( 160.0,  25.0);
    
    if([snMapRectangleCol2a containsSNMapRectangle:snMapRectangleINSIDE3])
    {
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    if([snMapRectangleCol2a containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    //COL 2
    
    //col2 top
    SNMapRectangle *snMapRectangleCol2b = [[SNMapRectangle alloc]init];
    snMapRectangleCol2b.pA = CLLocationCoordinate2DMake(-50.0, -50.0);
    snMapRectangleCol2b.pB = CLLocationCoordinate2DMake( 50.0, -50.0);
    snMapRectangleCol2b.pC = CLLocationCoordinate2DMake( 50.0,  50.0);
    snMapRectangleCol2b.pD = CLLocationCoordinate2DMake(-50.0,  50.0);
    
    
    SNMapRectangle *snMapRectangleINSIDE4 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE4.pA = CLLocationCoordinate2DMake( -10.0, -25.0);
    snMapRectangleINSIDE4.pB = CLLocationCoordinate2DMake(  10.0, -25.0);
    snMapRectangleINSIDE4.pC = CLLocationCoordinate2DMake(  10.0,  25.0);
    snMapRectangleINSIDE4.pD = CLLocationCoordinate2DMake( -10.0,  25.0);
    
    if([snMapRectangleCol2b containsSNMapRectangle:snMapRectangleINSIDE4])
    {
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    if([snMapRectangleCol2b containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    SNMapRectangle *snMapRectangleCol2c = [[SNMapRectangle alloc]init];
    snMapRectangleCol2c.pA = CLLocationCoordinate2DMake(-200.0, -50.0);
    snMapRectangleCol2c.pB = CLLocationCoordinate2DMake(-100.0, -50.0);
    snMapRectangleCol2c.pC = CLLocationCoordinate2DMake(-100.0,  50.0);
    snMapRectangleCol2c.pD = CLLocationCoordinate2DMake(-200.0,  50.0);
    
    
    SNMapRectangle *snMapRectangleINSIDE5 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE5.pA = CLLocationCoordinate2DMake( -180.0, -25.0);
    snMapRectangleINSIDE5.pB = CLLocationCoordinate2DMake( -160.0, -25.0);
    snMapRectangleINSIDE5.pC = CLLocationCoordinate2DMake( -160.0,  25.0);
    snMapRectangleINSIDE5.pD = CLLocationCoordinate2DMake( -180.0,   25.0);
    
    if([snMapRectangleCol2c containsSNMapRectangle:snMapRectangleINSIDE5])
    {
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    if([snMapRectangleCol2c containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    
    
    
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //COL 3
    
    

    //COL 2 - lng start + end +
    SNMapRectangle *snMapRectangleCol3a = [[SNMapRectangle alloc]init];
    snMapRectangleCol3a.pA = CLLocationCoordinate2DMake(100.0, 100.0);
    snMapRectangleCol3a.pB = CLLocationCoordinate2DMake(200.0, 100.0);
    snMapRectangleCol3a.pC = CLLocationCoordinate2DMake(200.0, 200.0);
    snMapRectangleCol3a.pD = CLLocationCoordinate2DMake(100.0, 200.0);
    
    
    SNMapRectangle *snMapRectangleINSIDE6 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE6.pA = CLLocationCoordinate2DMake( 160.0, 160.0);
    snMapRectangleINSIDE6.pB = CLLocationCoordinate2DMake( 180.0, 160.0);
    snMapRectangleINSIDE6.pC = CLLocationCoordinate2DMake( 180.0, 180.0);
    snMapRectangleINSIDE6.pD = CLLocationCoordinate2DMake( 160.0, 180.0);
    
    if([snMapRectangleCol3a containsSNMapRectangle:snMapRectangleINSIDE6])
    {
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    if([snMapRectangleCol3a containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    
    
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    SNMapRectangle *snMapRectangleCol3b = [[SNMapRectangle alloc]init];
    snMapRectangleCol3b.pA = CLLocationCoordinate2DMake(-50.0, 100.0);
    snMapRectangleCol3b.pB = CLLocationCoordinate2DMake( 50.0, 100.0);
    snMapRectangleCol3b.pC = CLLocationCoordinate2DMake( 50.0, 200.0);
    snMapRectangleCol3b.pD = CLLocationCoordinate2DMake(-50.0, 200.0);
    
    SNMapRectangle *snMapRectangleINSIDE7 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE7.pA = CLLocationCoordinate2DMake( -10.0, 160.0);
    snMapRectangleINSIDE7.pB = CLLocationCoordinate2DMake(  10.0, 160.0);
    snMapRectangleINSIDE7.pC = CLLocationCoordinate2DMake(  10.0, 180.0);
    snMapRectangleINSIDE7.pD = CLLocationCoordinate2DMake( -10.0, 180.0);
    
    if([snMapRectangleCol3b containsSNMapRectangle:snMapRectangleINSIDE7])
    {
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    if([snMapRectangleCol3b containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    
    }
    
    
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    SNMapRectangle *snMapRectangleCol3c = [[SNMapRectangle alloc]init];
    snMapRectangleCol3c.pA = CLLocationCoordinate2DMake(-200.0, 100.0);
    snMapRectangleCol3c.pB = CLLocationCoordinate2DMake(-100.0, 100.0);
    snMapRectangleCol3c.pC = CLLocationCoordinate2DMake(-100.0, 200.0);
    snMapRectangleCol3c.pD = CLLocationCoordinate2DMake(-200.0, 200.0);
    
    
    
    
    SNMapRectangle *snMapRectangleINSIDE8 = [[SNMapRectangle alloc]init];
    snMapRectangleINSIDE8.pA = CLLocationCoordinate2DMake( -180.0, 160.0);
    snMapRectangleINSIDE8.pB = CLLocationCoordinate2DMake( -160.0, 160.0);
    snMapRectangleINSIDE8.pC = CLLocationCoordinate2DMake( -160.0, 180.0);
    snMapRectangleINSIDE8.pD = CLLocationCoordinate2DMake( -180.0, 180.0);
    
    if([snMapRectangleCol3c containsSNMapRectangle:snMapRectangleINSIDE8])
    {
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
    }
    
    if([snMapRectangleCol3c containsSNMapRectangle:snMapRectangleCol1a]){
        if(_debugOn)NSLog(@"%s CONTAINED:YES", __PRETTY_FUNCTION__);
    }else{
        if(_debugOn)NSLog(@"%s CONTAINED:NO", __PRETTY_FUNCTION__);
        
    }
    
    
    
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    

    
}

-(void)testIsBetweenValueOne{
    BOOL result;
    //CASE 1/4
    //valueOne is +
    //valueTwo is +
    //TEST 1 - in range
    result = [self values:20 isBetweenValueOne:30.0 andValueTwo:10.0];
    result = [self values:20 isBetweenValueOne:10.0 andValueTwo:30.0];
    
    result = [self values:0.0 isBetweenValueOne:0.0 andValueTwo:10.0];
    
    
    //TEST 1 - not range
    result = [self values:5  isBetweenValueOne:30.0 andValueTwo:10.0];
    result = [self values:-40 isBetweenValueOne:30.0 andValueTwo:10.0];
    //-----------------------------------------------------------------------------------
    
    //CASE 2/4 - SHOULD NEVER HAPPEN - works because i swap v1 and v2
    //valueOne is +
    //valueTwo is -
    result = [self values:-10 isBetweenValueOne:30.0 andValueTwo:-10.0];
    
    
    

    //-----------------------------------------------------------------------------------
    //CASE 3/4
    //valueOne is -
    //valueTwo is +
    result = [self values:-10 isBetweenValueOne:-30.0 andValueTwo:10.0];
    
    result = [self values:0.0 isBetweenValueOne:-30.0 andValueTwo:0.0];
    
    
    //out of range
    result = [self values:-40 isBetweenValueOne:-30.0 andValueTwo:10.0];
    result = [self values:40 isBetweenValueOne:-30.0 andValueTwo:10.0];
    //-----------------------------------------------------------------------------------
    //CASE 4/4 - BOTH NEG
    //valueOne is -
    //valueTwo is -
    //OK
    result = [self values:-20 isBetweenValueOne:-30.0 andValueTwo:-10.0];

    
    
    //NO
    result = [self values:-40 isBetweenValueOne:-30.0 andValueTwo:-10.0];
    result = [self values:10 isBetweenValueOne:-30.0 andValueTwo:-10.0];

}
-(BOOL)values:(double)valueToCheck isBetweenValueOne:(double)valueOneIN andValueTwo:(double)valueTwoIN
{
    BOOL valueIsBetween_ = FALSE;
    
    
    double valueOne;
    double valueTwo;
    
    //valueOne should always be the lowest
    if(valueOneIN > valueTwoIN){
        //swap them
        valueOne = valueTwoIN;//valueOne always the lowest
        valueTwo = valueOneIN;
    }else{
        //use as entered
        valueOne = valueOneIN;
        valueTwo = valueTwoIN;
    }
    
    
    //calculations will vary depending on where on the number line the range is
    if(valueOne >= 0){
        //valueOne is +
        //-----------------------------------------------------------------------------------
        if(valueTwo >= 0){
            //CASE 1/4
            //valueOne is +
            //valueTwo is +
            
            //valueTwo is always higher
            if((valueToCheck >= valueOne) && (valueToCheck <= valueTwo)){
                valueIsBetween_ = TRUE;
            }else{
                valueIsBetween_ = FALSE;
            }
            
        }else{
            //CASE 2/4 - SHOULD NEVER HAPPEN
            //valueOne is +
            //valueTwo is -
            //we swap them so valueTwo always the highest
            NSLog(@"ERROR:<%@ %@:(%d)> %s SHOULD NEVER HAPPEN", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__);
        }
        //-----------------------------------------------------------------------------------
    }else{
        //valueOne is -
        if(valueTwo >= 0){
            //CASE 3/4
            //valueOne is -
            //valueTwo is +
            if(valueToCheck >= 0)
            {
                //valueToCheck is positive
                //valueTwo is +
                // so check range from 0 to valueTwo
                if((valueToCheck >= 0.0) && (valueToCheck <= valueTwo)){
                    valueIsBetween_ = TRUE;
                }else{
                    valueIsBetween_ = FALSE;
                }
            }else{
                //valueToCheck is -
                //valueOne is -
                //so check range from valueOne to 0
                if((valueToCheck >= valueOne) && (valueToCheck <= 0.0)){
                    valueIsBetween_ = TRUE;
                }else{
                    valueIsBetween_ = FALSE;
                }
            }
        }else{
            //CASE 4/4 - BOTH NEG
            //valueOne is -
            //valueTwo is -
            
            if(valueToCheck >= 0)
            {
                //valueToCheck is positive
                //valueOne is -
                //valueTwo is -
                //cant possibly be in range
                valueIsBetween_ = FALSE;
                
            }else{
                //valueToCheck is -
                
                //valueOne is -
                //valueTwo is -
                
                if((valueToCheck >= valueOne) && (valueToCheck <= valueTwo)){
                    valueIsBetween_ = TRUE;
                }else{
                    valueIsBetween_ = FALSE;
                }
            }
        }
    }
    return valueIsBetween_;
    
}



@end
