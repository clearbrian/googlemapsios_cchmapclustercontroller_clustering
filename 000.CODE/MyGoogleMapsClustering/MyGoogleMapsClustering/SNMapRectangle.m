//
//  SNMapRectangle.m
//  seanet_for_ios
//
//  Created by Brian Clear (gbxc) on 14/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import "SNMapRectangle.h"

@interface SNMapRectangle(){
    BOOL _debugOn;
}

@end


@implementation SNMapRectangle
- (id)init
{
    self = [super init];
    if (self) {
        _debugOn = TRUE;
    }
    return self;
}

-(BOOL)containsSNMapRectangle:(SNMapRectangle *)innerRectangle{
    //if any point is in the outer rectangle then we can say it contains it.
    //clusterGroup are just larger version of clusters so will all line up
    
    
    BOOL thisRectangleContainsInnerRectangle = FALSE;
    
    
    CLLocationDegrees minLat,minLng,maxLat,maxLng;
    //set it to first point - will be overwritten by next few tests
    minLat = self.pC.latitude;
    minLng = self.pC.longitude;
    maxLat = self.pC.latitude;
    maxLng = self.pC.longitude;
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, self.pC.latitude);
    minLng = MIN(minLng, self.pC.longitude);
    
    maxLat = MAX(maxLat, self.pC.latitude);
    maxLng = MAX(maxLng, self.pC.longitude);
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, self.pB.latitude);
    minLng = MIN(minLng, self.pB.longitude);
    
    maxLat = MAX(maxLat, self.pB.latitude);
    maxLng = MAX(maxLng, self.pB.longitude);
    //-----------------------------------------------------------------------------------
    minLat = MIN(minLat, self.pA.latitude);
    minLng = MIN(minLng, self.pA.longitude);
    
    maxLat = MAX(maxLat, self.pD.latitude);
    maxLng = MAX(maxLng, self.pD.longitude);
    //-----------------------------------------------------------------------------------
    
    if(_debugOn)NSLog(@"%s minLat:%f", __PRETTY_FUNCTION__, minLat);
    if(_debugOn)NSLog(@"%s maxLat:%f", __PRETTY_FUNCTION__, maxLat);

    if(_debugOn)NSLog(@"%s minLng:%f", __PRETTY_FUNCTION__, minLng);
    if(_debugOn)NSLog(@"%s maxLng:%f", __PRETTY_FUNCTION__, maxLng);

    
    BOOL isPointWithRange_pA = [self isPointWithinRange:innerRectangle.pA
                                              minLat:minLat
                                              maxLat:maxLat
                                              minLng:minLng
                                              maxLng:maxLng];
    
    BOOL isPointWithRange_pB = [self isPointWithinRange:innerRectangle.pB
                                                 minLat:minLat
                                                 maxLat:maxLat
                                                 minLng:minLng
                                                 maxLng:maxLng];

    BOOL isPointWithRange_pC = [self isPointWithinRange:innerRectangle.pC
                                                 minLat:minLat
                                                 maxLat:maxLat
                                                 minLng:minLng
                                                 maxLng:maxLng];

    BOOL isPointWithRange_pD = [self isPointWithinRange:innerRectangle.pD
                                                 minLat:minLat
                                                 maxLat:maxLat
                                                 minLng:minLng
                                                 maxLng:maxLng];

    // we cant say if ANY point is in the outer rect then its contained
    //as theres the case when a inner cluster is up against the larger cluster
    //then 4 of its point will be in a cluster
    //but two of its points will be in the next cluster as the sides meet.
    //in this app smaller clusters are scaled versions of larger ones so they will line up often
    
    //if any point is in the outer rectangle then we can say it contains it
    if(isPointWithRange_pA &&
       isPointWithRange_pB &&
       isPointWithRange_pC &&
       isPointWithRange_pD)
    {
        NSLog(@"[%s] ALL POINTS ARE IN RECT", __PRETTY_FUNCTION__);
        thisRectangleContainsInnerRectangle = TRUE;
    }else{
        NSLog(@"[%s] NOT ALL 4 POINTs IS IN RECT - 2 maybe if so then the inner cluster is outside but up against the outer cluster", __PRETTY_FUNCTION__);
        thisRectangleContainsInnerRectangle = FALSE;
    }

    return thisRectangleContainsInnerRectangle;
}

-(BOOL)isPointWithinRange:(CLLocationCoordinate2D) point
                                            minLat:(CLLocationDegrees) minLat
            maxLat:(CLLocationDegrees) maxLat
            minLng:(CLLocationDegrees) minLng
            maxLng:(CLLocationDegrees) maxLng
{
    BOOL isPointWithRange;
    BOOL latIsBetween_ = [self values:point.latitude
                    isBetweenValueOne:minLat
                          andValueTwo:maxLat];
    
    BOOL lngIsBetween_ = [self values:point.longitude
                    isBetweenValueOne:minLng
                          andValueTwo:maxLng];
    
    if(latIsBetween_)
    {
        if(_debugOn)NSLog(@"%s latIsBetween_:YES", __PRETTY_FUNCTION__);
        if(lngIsBetween_)
        {
            if(_debugOn)NSLog(@"%s lngIsBetween_:YES", __PRETTY_FUNCTION__);
            //both lat and lng within this rectangle
            if(_debugOn)NSLog(@"%s POINT IN RECT:YES", __PRETTY_FUNCTION__);
            isPointWithRange = YES;
        }else{
            if(_debugOn)NSLog(@"%s lngIsBetween_:NO", __PRETTY_FUNCTION__);
            if(_debugOn)NSLog(@"%s POINT IN RECT:NO", __PRETTY_FUNCTION__);
            isPointWithRange = NO;
            
        }
    }else{
        if(_debugOn)NSLog(@"%s latIsBetween_:NO", __PRETTY_FUNCTION__);
        if(lngIsBetween_)
        {
            if(_debugOn)NSLog(@"%s lngIsBetween_:YES", __PRETTY_FUNCTION__);
            if(_debugOn)NSLog(@"%s POINT IN RECT:NO", __PRETTY_FUNCTION__);
            isPointWithRange = NO;
        }else{
            if(_debugOn)NSLog(@"%s lngIsBetween_:NO", __PRETTY_FUNCTION__);
            if(_debugOn)NSLog(@"%s POINT IN RECT:NO", __PRETTY_FUNCTION__);
            isPointWithRange = NO;
        }
    }
    return isPointWithRange;
}

-(void)testIsBetweenValueOne{
    BOOL result;
    //CASE 1/4
    //valueOne is +
    //valueTwo is +
    //TEST 1 - in range
    result = [self values:20 isBetweenValueOne:30.0 andValueTwo:10.0];
    result = [self values:20 isBetweenValueOne:10.0 andValueTwo:30.0];
    
    result = [self values:0.0 isBetweenValueOne:0.0 andValueTwo:10.0];
    
    
    //TEST 1 - not range
    result = [self values:5  isBetweenValueOne:30.0 andValueTwo:10.0];
    result = [self values:-40 isBetweenValueOne:30.0 andValueTwo:10.0];
    //-----------------------------------------------------------------------------------
    
    //CASE 2/4 - SHOULD NEVER HAPPEN - works because i swap v1 and v2
    //valueOne is +
    //valueTwo is -
    result = [self values:-10 isBetweenValueOne:30.0 andValueTwo:-10.0];
    
    
    
    
    //-----------------------------------------------------------------------------------
    //CASE 3/4
    //valueOne is -
    //valueTwo is +
    result = [self values:-10 isBetweenValueOne:-30.0 andValueTwo:10.0];
    
    result = [self values:0.0 isBetweenValueOne:-30.0 andValueTwo:0.0];
    
    
    //out of range
    result = [self values:-40 isBetweenValueOne:-30.0 andValueTwo:10.0];
    result = [self values:40 isBetweenValueOne:-30.0 andValueTwo:10.0];
    //-----------------------------------------------------------------------------------
    //CASE 4/4 - BOTH NEG
    //valueOne is -
    //valueTwo is -
    //OK
    result = [self values:-20 isBetweenValueOne:-30.0 andValueTwo:-10.0];
    
    
    
    //NO
    result = [self values:-40 isBetweenValueOne:-30.0 andValueTwo:-10.0];
    result = [self values:10 isBetweenValueOne:-30.0 andValueTwo:-10.0];
    
}
-(BOOL)values:(double)valueToCheck isBetweenValueOne:(double)valueOneIN andValueTwo:(double)valueTwoIN
{
    BOOL valueIsBetween_ = FALSE;
    
    
    double valueOne;
    double valueTwo;
    
    //valueOne should always be the lowest
    if(valueOneIN > valueTwoIN){
        //swap them
        valueOne = valueTwoIN;//valueOne always the lowest
        valueTwo = valueOneIN;
    }else{
        //use as entered
        valueOne = valueOneIN;
        valueTwo = valueTwoIN;
    }
    
    
    //calculations will vary depending on where on the number line the range is
    if(valueOne >= 0){
        //valueOne is +
        //-----------------------------------------------------------------------------------
        if(valueTwo >= 0){
            //CASE 1/4
            //valueOne is +
            //valueTwo is +
            
            //valueTwo is always higher
            if((valueToCheck >= valueOne) && (valueToCheck <= valueTwo)){
                valueIsBetween_ = TRUE;
            }else{
                valueIsBetween_ = FALSE;
            }
            
        }else{
            //CASE 2/4 - SHOULD NEVER HAPPEN
            //valueOne is +
            //valueTwo is -
            //we swap them so valueTwo always the highest
            NSLog(@"ERROR:<%@ %@:(%d)> %s SHOULD NEVER HAPPEN", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__);
        }
        //-----------------------------------------------------------------------------------
    }else{
        //valueOne is -
        if(valueTwo >= 0){
            //CASE 3/4
            //valueOne is -
            //valueTwo is +
            if(valueToCheck >= 0)
            {
                //valueToCheck is positive
                //valueTwo is +
                // so check range from 0 to valueTwo
                if((valueToCheck >= 0.0) && (valueToCheck <= valueTwo)){
                    valueIsBetween_ = TRUE;
                }else{
                    valueIsBetween_ = FALSE;
                }
            }else{
                //valueToCheck is -
                //valueOne is -
                //so check range from valueOne to 0
                if((valueToCheck >= valueOne) && (valueToCheck <= 0.0)){
                    valueIsBetween_ = TRUE;
                }else{
                    valueIsBetween_ = FALSE;
                }
            }
        }else{
            //CASE 4/4 - BOTH NEG
            //valueOne is -
            //valueTwo is -
            
            if(valueToCheck >= 0)
            {
                //valueToCheck is positive
                //valueOne is -
                //valueTwo is -
                //cant possibly be in range
                valueIsBetween_ = FALSE;
                
            }else{
                //valueToCheck is -
                
                //valueOne is -
                //valueTwo is -
                
                if((valueToCheck >= valueOne) && (valueToCheck <= valueTwo)){
                    valueIsBetween_ = TRUE;
                }else{
                    valueIsBetween_ = FALSE;
                }
            }
        }
    }
    return valueIsBetween_;
    
}
@end
