//
//  SNMapRectangle.h
//  seanet_for_ios
//
//  Created by Brian Clear (gbxc) on 14/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SNMapRectangle : NSObject

@property (nonatomic, assign) CLLocationCoordinate2D pA;
@property (nonatomic, assign) CLLocationCoordinate2D pB;
@property (nonatomic, assign) CLLocationCoordinate2D pC;
@property (nonatomic, assign) CLLocationCoordinate2D pD;
@property (nonatomic, assign) CLLocationCoordinate2D centerPoint;

-(BOOL)containsSNMapRectangle:(SNMapRectangle *)innerRectangle;
@end
