//
//  COLCSecondViewController.m
//  MyGoogleMapsClustering
//
//  Created by Brian Clear (gbxc) on 13/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import "COLCPanGestureEndGoogleMapsViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "SNGMSMapView.h"
@interface COLCPanGestureEndGoogleMapsViewController ()<SNGMSMapViewDelegate>{
    BOOL _debugOn;
    
    
}
@property (weak, nonatomic) IBOutlet SNGMSMapView *mapView;
@property (nonatomic, retain)  NSTimer *moveMapRedrawClustersTimer;
@end

@implementation COLCPanGestureEndGoogleMapsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _debugOn = TRUE;
    
    self.mapView.snGMSMapViewDelegate = self;
    
}

- (void)snGMSMapView:(SNGMSMapView *)snGMSMapView idleAtCameraPosition:(GMSCameraPosition *)position{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
}


//so use gestureHandler and check for UIGestureRecognizerStateEnded - this is when user had lifted their finger/ended the pan
- (void)snGMSMapView:(SNGMSMapView *)snGMSMapView  panGestureHandler:(UIPanGestureRecognizer *)recognizer{
    if([recognizer state] == UIGestureRecognizerStatePossible)
    {
        if(_debugOn)NSLog(@"UIGestureRecognizerStatePossible");
    }
    else if([recognizer state] == UIGestureRecognizerStateBegan)
    {
        if(_debugOn)NSLog(@"UIGestureRecognizerStateBegan");
    }
    else if([recognizer state] == UIGestureRecognizerStateChanged)
    {
        if(_debugOn)NSLog(@"UIGestureRecognizerStateChanged");
    }
    else if([recognizer state] == UIGestureRecognizerStateEnded)
    {
        if(_debugOn)NSLog(@"UIGestureRecognizerStateEnded");
        
        //BEST PLACE TO TEST IF USER HAS LIFTED THEIR FINGER
        
        
        //-----------------------------------------------------------------------------------
        //wait one second before redrawing markers/overlays if case user has briefly lifted their finger
        //repeats:NO so no need to add code to invalidate it
        self.moveMapRedrawClustersTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                                           target:self
                                                                         selector:@selector(timerFired:)
                                                                         userInfo:nil
                                                                          repeats:NO];
        
        //-----------------------------------------------------------------------------------
        
    }
    else if([recognizer state] == UIGestureRecognizerStateCancelled)
    {
        if(_debugOn)NSLog(@"UIGestureRecognizerStateCancelled");
    }
    else if([recognizer state] == UIGestureRecognizerStateFailed)
    {
        if(_debugOn)NSLog(@"UIGestureRecognizerStateFailed");
    }
    else{
        NSLog(@"ERROR:<%@ %@:(%d)> %s UNHANDLED STATE", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__);
    }
    
    
    
}

#pragma mark -
#pragma mark MAP STOPPED MOVING - REDRAW VISIBLE CLUSTERS
#pragma mark -

- (void)timerFired:(NSTimer*)theTimer {
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    //USER HAS LIFTED THEIR FINGER and time period has passed so safe to redraw markers on the map
  
}



- (void)viewDidDisappear:(BOOL)animated {

    [self.moveMapRedrawClustersTimer invalidate];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self.moveMapRedrawClustersTimer invalidate];
}

@end
