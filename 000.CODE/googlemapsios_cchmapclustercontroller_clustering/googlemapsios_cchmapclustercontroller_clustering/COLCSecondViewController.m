//
//  COLCSecondViewController.m
//  googlemapsios_cchmapclustercontroller_clustering
//
//  Created by Brian Clear (gbxc) on 12/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//
#import <GoogleMaps/GoogleMaps.h>
#import "COLCSecondViewController.h"

#import "CCHMapClusterControllerForGoogleMaps.h"
#import "CCHMapClusterControllerForGoogleMapsDelegate.h"

#import "DataReaderForGoogleMaps.h"
#import "DataReaderForGoogleMapsDelegate.h"


#import "Settings.h"

#import "CCHMapClusterAnnotationForGoogleMaps.h"
#import "ClusterAnnotationView.h"

#import "CCHCenterOfMassMapClustererForGoogleMaps.h"
#import "CCHNearCenterMapClustererForGoogleMaps.h"
#import "CCHFadeInOutMapAnimatorForGoogleMaps.h"

#import "GMSMapView+MKMapViewMethods.h"

@interface COLCSecondViewController ()<CCHMapClusterControllerForGoogleMapsDelegate, DataReaderForGoogleMapsDelegate>{
    BOOL _debugOn;
}
@property (weak, nonatomic) IBOutlet GoogleGMSMapView *mapView;

@property (strong, nonatomic) CCHMapClusterControllerForGoogleMaps *mapClusterController;
@property (strong, nonatomic) DataReaderForGoogleMaps *dataReader;

@property (strong, nonatomic) Settings *settings;

@property (strong, nonatomic) id<CCHMapClustererForGoogleMaps> mapClusterer;
@property (strong, nonatomic) id<CCHMapAnimatorForGoogleMaps> mapAnimator;

@end

@implementation COLCSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-37.81969
                                                            longitude:144.966085
                                                                 zoom:4];
//    self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    
    
    //-----------------------------------------------------------------------------------
    _debugOn = TRUE;
    
    
	// Do any additional setup after loading the view, typically from a nib.
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    // Set up map clustering
    
    //MAKE SURE ITS SET TO GoogleGMSMapView in IB not GSMMapView
    self.mapClusterController = [[CCHMapClusterControllerForGoogleMaps alloc] initWithMapView:self.mapView];
    self.mapClusterController.delegate = self;
    
    // Read annotations
    self.dataReader = [[DataReaderForGoogleMaps alloc] init];
    self.dataReader.delegate = self;
    
    // Settings
    [self resetSettings];
}

- (IBAction)resetSettings
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    Settings *settings = [[Settings alloc] init];
    [self updateWithSettings:settings];
}

- (void)updateWithSettings:(Settings *)settings
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    self.settings = settings;
    
    // Reset
    [self.dataReader stopReadingData];
    [self.mapClusterController removeAnnotations:self.mapClusterController.annotations.allObjects withCompletionHandler:NULL];
   
    //-----------------------------------------------------------------------------------
    //for (id<MKOverlay> overlay in self.mapView.overlays) {
    //    [self.mapView removeOverlay:overlay];
    //}
    [self.mapView clear];
    
    // Map cluster controller settings
    self.mapClusterController.debuggingEnabled = settings.isDebuggingEnabled;
    self.mapClusterController.cellSize = settings.cellSize;
    self.mapClusterController.marginFactor = settings.marginFactor;
    
    if (settings.clusterer == SettingsClustererCenterOfMass) {
        self.mapClusterer = [[CCHCenterOfMassMapClustererForGoogleMaps alloc] init];
    } else if (settings.clusterer == SettingsClustererNearCenter) {
        self.mapClusterer = [[CCHNearCenterMapClustererForGoogleMaps alloc] init];
    }
    self.mapClusterController.clusterer = self.mapClusterer;
    
    if (settings.animator == SettingsAnimatorFadeInOut) {
        self.mapAnimator = [[CCHFadeInOutMapAnimatorForGoogleMaps alloc] init];
    }
    self.mapClusterController.animator = self.mapAnimator;
    
    // Region and data
    MKCoordinateRegion region;
    if (self.settings.dataSet == SettingsDataSetBerlin) {
        // 5000+ items near Berlin
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(52.516221, 13.377829);
        region = MKCoordinateRegionMakeWithDistance(location, 45000, 45000);
        [self.dataReader startReadingBerlinData];
    } else {
        // 80000+ items in the US
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(39.833333, -98.583333);
        region = MKCoordinateRegionMakeWithDistance(location, 7000000, 7000000);
        [self.dataReader startReadingUSData];
    }
    #pragma mark TODO -
    //self.mapView.region = region;
}

#pragma mark -
#pragma mark <#METHODS#>
#pragma mark -

- (void)didReceiveMemoryWarning
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark DataReaderDelegate
#pragma mark -

- (void)dataReader:(DataReaderForGoogleMaps *)dataReader addAnnotations:(NSArray *)annotations
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    [self.mapClusterController addAnnotations:annotations withCompletionHandler:NULL];
}

#pragma mark -
#pragma mark CCHMapClusterControllerDelegate
#pragma mark -
- (NSString *)mapClusterController:(CCHMapClusterControllerForGoogleMaps *)mapClusterController
      titleForMapClusterAnnotation:(CCHMapClusterAnnotationForGoogleMaps *)mapClusterAnnotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    NSUInteger numAnnotations = mapClusterAnnotation.annotations.count;
    NSString *unit = numAnnotations > 1 ? @"annotations" : @"annotation";
    return [NSString stringWithFormat:@"%tu %@", numAnnotations, unit];
}

- (NSString *)mapClusterController:(CCHMapClusterControllerForGoogleMaps *)mapClusterController
   subtitleForMapClusterAnnotation:(CCHMapClusterAnnotationForGoogleMaps *)mapClusterAnnotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    
    NSUInteger numAnnotations = MIN(mapClusterAnnotation.annotations.count, 5);
    
    NSArray *annotations = [mapClusterAnnotation.annotations.allObjects subarrayWithRange:NSMakeRange(0, numAnnotations)];
    NSArray *titles = [annotations valueForKey:@"title"];
    return [titles componentsJoinedByString:@", "];
}

- (void)mapClusterController:(CCHMapClusterControllerForGoogleMaps *)mapClusterController
willReuseMapClusterAnnotation:(CCHMapClusterAnnotationForGoogleMaps *)mapClusterAnnotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    
    ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[self.mapClusterController.mapView viewForAnnotation:mapClusterAnnotation];
    
    clusterAnnotationView.count = mapClusterAnnotation.annotations.count;
}


#pragma mark -
#pragma mark MKMapViewDelegate
#pragma mark -

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if(_debugOn)NSLog(@"%s ", __PRETTY_FUNCTION__);
    MKAnnotationView *annotationView;
    
    if ([annotation isKindOfClass:CCHMapClusterAnnotationForGoogleMaps.class]) {
        static NSString *identifier = @"clusterAnnotation";
        
        ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (clusterAnnotationView) {
            clusterAnnotationView.annotation = annotation;
        } else {
            clusterAnnotationView = [[ClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            clusterAnnotationView.canShowCallout = YES;
        }
        
        CCHMapClusterAnnotationForGoogleMaps *clusterAnnotation = (CCHMapClusterAnnotationForGoogleMaps *)annotation;
        clusterAnnotationView.count = clusterAnnotation.annotations.count;
        annotationView = clusterAnnotationView;
    }
    
    return annotationView;
}

@end
