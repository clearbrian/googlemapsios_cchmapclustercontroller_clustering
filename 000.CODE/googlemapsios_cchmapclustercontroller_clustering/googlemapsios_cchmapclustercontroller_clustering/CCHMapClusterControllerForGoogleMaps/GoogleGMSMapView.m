//
//  GoogleGMSMapView.m
//  googlemapsios_cchmapclustercontroller_clustering
//
//  Created by Brian Clear (gbxc) on 12/02/2014.
//  Copyright (c) 2014 Brian Clear (gbxc). All rights reserved.
//

#import "GoogleGMSMapView.h"

@interface GoogleGMSMapView(){
    BOOL _debugOn;
}

@end


@implementation GoogleGMSMapView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _debugOn = TRUE;

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)addAnnotation:(id < MKAnnotation >)annotation{
     NSLog(@"%s TODO", __PRETTY_FUNCTION__);
}

- (void)addAnnotations:(NSArray *)annotations{

    NSLog(@"%s annotations:%d", __PRETTY_FUNCTION__, [annotations count]);
    
    NSLog(@"%s TODO:", __PRETTY_FUNCTION__);
    
    
    _annotations = annotations;
   
}
- (void)removeAnnotations:(NSArray *)annotations{
    NSLog(@"%s annotations:%d", __PRETTY_FUNCTION__, [annotations count]);
    NSLog(@"%s TODO:", __PRETTY_FUNCTION__);
    
}

- (MKAnnotationView *)viewForAnnotation:(id <MKAnnotation>)annotation{
    
     NSLog(@"%s TODO", __PRETTY_FUNCTION__);
    MKAnnotationView *  mkAnnotationView_ = nil;
    return mkAnnotationView_;
}

- (void)deselectAnnotation:(id < MKAnnotation >)annotation animated:(BOOL)animated{
     NSLog(@"%s TODO", __PRETTY_FUNCTION__);
    
}
- (NSSet *)annotationsInMapRect:(MKMapRect)mapRect{
         NSLog(@"%s TODO", __PRETTY_FUNCTION__);
    NSSet * set_ = nil;
    return set_;
}
- (void)setRegion:(MKCoordinateRegion)region animated:(BOOL)animated;{
     NSLog(@"%s TODO", __PRETTY_FUNCTION__);
    
}
- (CLLocationCoordinate2D)convertPoint:(CGPoint)point toCoordinateFromView:(UIView *)view{
     NSLog(@"%s TODO", __PRETTY_FUNCTION__);
    CLLocationCoordinate2D clLocationCoordinate2D_;
    return clLocationCoordinate2D_;
}
- (void)selectAnnotation:(id < MKAnnotation >)annotation animated:(BOOL)animated{
     NSLog(@"%s TODO", __PRETTY_FUNCTION__);
}
- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated{
     NSLog(@"%s TODO", __PRETTY_FUNCTION__);
}
//@property (nonatomic) MKMapRect visibleMapRect;

-(void)setVisibleMapRect:(MKMapRect)visibleMapRect{
    visibleMapRect = visibleMapRect;
    
}

@end
