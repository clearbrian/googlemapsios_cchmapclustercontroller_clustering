//
//  DataReaderForGoogleMapsDelegate.h
//  Macoun 2013
//
//  Created by Hoefele, Claus(choefele) on 20.09.13.
//  Copyright (c) 2013 Hoefele, Claus(choefele). All rights reserved.
//

#import <Foundation/Foundation.h>

@class DataReaderForGoogleMaps;

@protocol DataReaderForGoogleMapsDelegate <NSObject>

- (void)dataReader:(DataReaderForGoogleMaps *)dataReader addAnnotations:(NSArray *)annotations;

@end
