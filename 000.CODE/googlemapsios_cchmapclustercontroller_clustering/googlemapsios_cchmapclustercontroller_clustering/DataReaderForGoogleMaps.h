//
//  DataReaderForGoogleMaps.h
//  Macoun 2013
//
//  Created by Hoefele, Claus(choefele) on 20.09.13.
//  Copyright (c) 2013 Hoefele, Claus(choefele). All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataReaderForGoogleMapsDelegate;

@interface DataReaderForGoogleMaps : NSObject

@property (nonatomic, weak) id<DataReaderForGoogleMapsDelegate> delegate;

- (void)startReadingBerlinData;
- (void)startReadingUSData;
- (void)stopReadingData;

@end
